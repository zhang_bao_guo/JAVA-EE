package com.zbg.day16_map;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-03
 **/
public class PropertiesTest {
   public static void main(String[] args) throws Exception {
      Properties properties = new Properties();
      properties.load(new FileInputStream("src/main/name.properties"));
      String name = properties.getProperty("name");
      System.out.println(name);
   }
}
