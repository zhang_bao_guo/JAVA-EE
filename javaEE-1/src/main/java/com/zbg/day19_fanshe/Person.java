package com.zbg.day19_fanshe;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-11
 **/
public class Person {
   private String name;
   public Integer age;
   static int m = 10;

   public Person() {
      System.out.println("调用Person空参构造器");
   }

   public Person(String name, Integer age) {
      this.name = name;
      this.age = age;
   }

   private Person(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Integer getAge() {
      return age;
   }

   public void setAge(Integer age) {
      this.age = age;
   }

   public void show() {
      System.out.println("你好，我是一个人");
   }
   public void show(String str) {
      System.out.println("你好,我是"+str);
   }

   private String showNation(String nation) {
      System.out.println("我的国籍" + nation);
      return nation;
   }

   @Override
   public String toString() {
      return "Person{" +
         "name='" + name + '\'' +
         ", age=" + age +
         '}';
   }
}
