package com.zbg.day19_fanshe;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Random;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-16
 **/
public class NewInstanceTest {
  @Test
  public void Test1() throws Exception {
    Class<Person> clzz = Person.class;
    Person person = clzz.newInstance();
    Field name = clzz.getDeclaredField("name");
    name.setAccessible(true);
    name.set(person, "zhangsan");
    System.out.println(person);
  }

  @Test
  public void Test2() throws Exception {
    int i = new Random().nextInt(3);
    String classPath="";
    switch (i){
      case 0:
        classPath="java.util.Date";
        break;
      case 1:
        classPath="java.lang.Object";
        break;
      case 2:
        classPath="com.zbg.day19_fanshe.Person";
        break;
    }
    Object newInstance = getNewInstance(classPath);
    System.out.println(newInstance);
  }
  public Object getNewInstance(String classPath) throws Exception {
    System.out.println(classPath);
    Class clazz = Class.forName(classPath);
    return clazz.newInstance();
  }
}
