package com.zbg.day19_fanshe;

import org.junit.Test;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-11
 **/
public class ClassLoaderTest {
  @Test
  public void Test01() throws Exception {
    Class<Person> clazz = Person.class;
    // 1:通过反射调用构造器创建对象
    Constructor<Person> con = clazz.getConstructor(String.class, Integer.class);
    Person p1 = con.newInstance("Tom", 12);
    System.out.println(p1);
    // 2:通过反射调用指定的属性和方法 getDeclaredField
    Field age = clazz.getDeclaredField("age");
    age.set(p1, 10);
    System.out.println(p1);
    Method show = clazz.getDeclaredMethod("show");
    show.invoke(p1);

    Method show1 = clazz.getDeclaredMethod("show", String.class);
    show1.invoke(p1,"中国人");

    System.out.println("*******************");

    Constructor<Person> con1 = clazz.getDeclaredConstructor(String.class);
    con1.setAccessible(true);
    Person p2 = con1.newInstance("Jeck");
    System.out.println(p2);
  }

  @Test
  public void Test02() throws Exception {
    Class<Person> clazz = Person.class;
    Constructor<Person> cons1 = clazz.getDeclaredConstructor(String.class, Integer.class);
    Person p1 = cons1.newInstance("P", 12);
    System.out.println(p1);
    Field m = clazz.getDeclaredField("m");
    System.out.println(m.get(p1));
    Field name = clazz.getDeclaredField("name");
    name.setAccessible(true);
    System.out.println(name.get(p1));
    name.set(p1, "p2");
    System.out.println(p1);
    Method show = clazz.getDeclaredMethod("show");
    show.invoke(p1);
    Method showNation = clazz.getDeclaredMethod("showNation", String.class);
    showNation.setAccessible(true);
    showNation.invoke(p1, "中国");
    Method setName = clazz.getDeclaredMethod("setName", String.class);
    setName.invoke(p1, "张三");
    System.out.println(p1);
  }

  @Test
  public void Test03() throws Exception {
    Class<Person> clazz = Person.class;
    System.out.println(clazz);

    Person person = new Person();
    Class<? extends Person> clazz1 = person.getClass();
    System.out.println(clazz1);

    Class clazz2 = Class.forName("com.zbg.day19_fanshe.Person");
    System.out.println(clazz2);
    System.out.println(clazz == clazz1);
    System.out.println(clazz == clazz2);

    Constructor d = clazz2.getDeclaredConstructor(String.class, Integer.class);
    Object ym = d.newInstance("Ym", 112);
    System.out.println(ym);
    Field name = clazz2.getDeclaredField("name");
    name.setAccessible(true);
    System.out.println(name.get(ym));


    ClassLoader classLoader = Person.class.getClassLoader();
    Class<?> clazz3 = classLoader.loadClass("com.zbg.day19_fanshe.Person");
    Constructor<?> declaredConstructor1 = clazz3.getDeclaredConstructor(String.class, Integer.class);
    Object jk1 = declaredConstructor1.newInstance("jk", 2);
    System.out.println(clazz3);
    System.out.println(clazz == clazz3);
    ClassLoader parent = classLoader.getParent();
    Class<?> aClass = parent.loadClass("com.zbg.day19_fanshe.Person");
    Constructor<?> declaredConstructor = aClass.getDeclaredConstructor(String.class, Integer.class);
    Object jk = declaredConstructor.newInstance("jk", 2);
    System.out.println(jk);
  }

  @Test
  public void Test3() throws Exception {
    Properties properties = new Properties();
    //properties.load(new FileReader("D:\\06.IDEAWK\\javaEE\\day01\\Test.properties"));
    ClassLoader classLoader = ClassLoaderTest.class.getClassLoader();
    //该方式默认加载src文件目录下的配置文件
    InputStream is = classLoader.getResourceAsStream("D:\\06.IDEAWK\\javaEE\\day01\\src\\qerr.properties");
    properties.load(is);
    String user = properties.getProperty("user");
    System.out.println(user);
  }
}
