package com.zbg.day13_List;

import com.zbg.day03_OOP.Person;
import org.junit.Test;

import java.util.*;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-15
 **/
public class Conactions {
    @Test
    public void test01() {
        List<Object> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.remove("2");
        String[] str = {"1", "21", "2"};
        list.forEach(System.out::println);

        Map map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        map.put("c", 3);
        map.put("d", 4);
        Set set = map.keySet();
        Iterator list1 = set.iterator();
        while (list1.hasNext()) {
            Object next = list1.next();
            Object o = map.get(next);
            System.out.println(next + "=" + o);
        }
    }

    @Test
    public void test02() {
        List coll1 = Arrays.asList("1", 123, 234, 4567);

        Collection coll = new ArrayList();
        coll.add("1");
        coll.add(123);
        coll.add(234);
        coll.add(756);
        boolean remove = coll.removeAll(coll1); // 差集：返回的是 coll1和coll的差集
        System.out.println(remove);
        System.out.println(coll);
    }

    @Test
    public void Test03() {
        Collection coll1 = new ArrayList();
        coll1.add("1");
        coll1.add(123);
        coll1.add(234);
        coll1.add(756);

        Collection coll = new ArrayList();
        coll.add("1");
        coll.add(123);
        coll.add(234);
        coll.add(new Person());
        coll.add(534);

        boolean remove = coll.retainAll(coll1);// 交集：返回的是 coll1和coll的交集
        System.out.println(remove);
        System.out.println(coll);
    }

    @Test
    public void Test04() {
        Collection coll1 = new ArrayList();
        coll1.add("1");
        coll1.add(123);
        coll1.add(234);
        coll1.add(756);

        Object[] objects = coll1.toArray();
        for (int i = objects.length - 1; i >= 0; i--) {
            System.out.println(objects[i]);
        }
        System.out.println();
        List<Object> objects1 = Arrays.asList(objects);
        for (int i = 0; i < objects1.size(); i++) {
            Object o = objects1.get(i);
            System.out.println(o);
        }
    }

    @Test
    public void Test05() {
        Collection<Integer> coll1 = new ArrayList();
        coll1.add(123);
        coll1.add(234);
        coll1.add(756);

        Iterator<Integer> iterator = coll1.iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (next == 123) {
                iterator.remove();
            }
            System.out.println(next);
        }
        for (Integer integer : coll1) {
            System.out.println(integer);
        }
    }

}
