package com.zbg.day13_List;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-16
 **/
public class ListTest {
    @Test
    public void Test01() {
        List linkList = new LinkedList();
        linkList.add(1);
        linkList.add(2);
        linkList.add(4);
        linkList.add(5);
        linkList.add(6);
        linkList.add(7);
        for (Object o : linkList) {
            System.out.println(o);
        }
        System.out.println();
        linkList.add(2, 3);
        for (Object o : linkList) {
            System.out.println(o);
        }
    }

    @Test
    public void Test02() {
    }
}
