package com.zbg.day13_List;

import org.junit.Test;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-20
 **/
public class ListTest01 {
  @Test
  public void say(){
    for (int i = 1; i < 10 ; i++) {
      for (int j = 1; j <= i; j++) {
        System.out.print(j+"*"+i+"="+j*i+" ");
      }
      System.out.println();
    }
  }

}
