package com.zbg.day06;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-12-30
 **/
public class Account {
    private int id;
    private String pwd;
    private double banlance;
    private static double interestRate;
    private static double minMoney = 1.0;

    private static int init = 1001;

    public Account() {
        id = init++;
    }

    public Account(String pwd, double banlance) {
        this.pwd = pwd;
        this.banlance = banlance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public double getBanlance() {
        return banlance;
    }

    public void setBanlance(double banlance) {
        this.banlance = banlance;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public static double getMinMoney() {
        return minMoney;
    }

    public static void setMinMoney(double minMoney) {
        Account.minMoney = minMoney;
    }

    public static int getInit() {
        return init;
    }

    public static void setInit(int init) {
        Account.init = init;
    }

    public static Account account = null;

    public static Account getAccount() {
        synchronized (Account.class) {
            if (account==null){
                account=new Account();
            }
        }
        return account;
    }
    @Override
    public String toString() {
        return "Account{" +
            "id=" + id +
            ", pwd='" + pwd + '\'' +
            ", banlance=" + banlance +
            '}';
    }
}
