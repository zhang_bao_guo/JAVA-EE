package com.zbg.day07_easyFactory;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-03
 **/
public interface ICar {
    public void run();
}
