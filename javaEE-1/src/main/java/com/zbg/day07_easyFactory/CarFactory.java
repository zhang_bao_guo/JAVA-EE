package com.zbg.day07_easyFactory;

import java.util.Optional;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-03
 **/
public class CarFactory {
    public static ICar getCar(String type) {
        switch (type) {
            case "audi":
                return new Audi();
            case "byd":
                return new Byd();
            default:
                return null;
        }
    }
}

class Test001{
    public static void main(String[] args) {
        ICar iCar = CarFactory.getCar("audi");
        ICar iCar1 = Optional.ofNullable(iCar).orElse(iCar);
        System.out.println(iCar1);
    }
}