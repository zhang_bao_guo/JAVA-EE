package com.zbg.day07_easyFactory;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-03
 **/
class Audi implements ICar {
    @Override
    public void run() {
        System.out.println("奥迪我在跑");
    }
}

class Byd implements ICar {
    @Override
    public void run() {
        System.out.println("比亚迪我在跑");
    }
}