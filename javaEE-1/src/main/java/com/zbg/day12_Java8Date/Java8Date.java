package com.zbg.day12_Java8Date;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-15
 **/
public class Java8Date {
    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
//        String format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(now);

        LocalDateTime of = LocalDateTime.of(2010, 11, 13, 11, 29, 25);
//        String format1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(of);
//        int i = format1.compareTo(format);
//        System.out.println(i);
        boolean after = of.isBefore(now);
        System.out.println(after);
    }

    @Test
    public void Test01() {
        Instant now = Instant.now();
        System.out.println(now);
        Date date = new Date();
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        System.out.println(format);
        LocalDateTime now1 = LocalDateTime.now();
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(now1));

    }

    @Test
    public void Test02() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime of = LocalDateTime.of(2020, 1, 15, 14, 20, 25);
        Duration between = Duration.between(now, of);
        System.out.println(between.getSeconds());
    }
    @Test
    public void Test03(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String format = dateTimeFormatter.format(now);
        System.out.println(format);
    }
    @Test
    public void Test04(){
        LocalDateTime now = LocalDateTime.now();
        int year = now.getYear();
        int dayOfYear = now.getDayOfYear();
        int monthValue = now.getMonthValue();
        System.out.println(year);
        System.out.println(dayOfYear);
        System.out.println(monthValue);
    }
    @Test
    public void Test05(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime localDateTime = now.plusMinutes(10);
        System.out.println(localDateTime);
    }
    @Test
    public void Test06(){
        LocalDate now = LocalDate.now();
        LocalTime now1 = LocalTime.now();
        LocalDateTime now2 = LocalDateTime.now();
        System.out.println(now);
        System.out.println(now1);
        System.out.println(now2);
    }
}
