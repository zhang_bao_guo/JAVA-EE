package com.zbg.day03_OOP;

import java.util.Arrays;
import java.util.Optional;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-12-02
 **/

class PersonTest{
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

public class Person {
    public static void main(String[] args) {
        PersonTest personTest = new PersonTest();
        String [] str = new String[3];
        Optional<String> s = Optional.ofNullable(str[1]);

        System.out.println(s);
        System.out.println(str[1]);

        String st = "123,123,432，123";
        String replace = st.replace(",", "，");
        String [] split = replace.split("，");
        System.out.println(split.length);
        System.out.println(Arrays.toString(split));

    }
}
