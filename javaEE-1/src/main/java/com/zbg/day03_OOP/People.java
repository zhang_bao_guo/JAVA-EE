package com.zbg.day03_OOP;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-12-17
 **/
public class People {
    String name;
    String sex;

    public People() {
    }

    public People(String name, String sex) {
        this.name = name;
        this.sex = sex;
    }

    public void eat() {
        System.out.println("我吃饭");
    }

    public void walk() {
        System.out.println("我跑步");
    }
}

class Man extends People {
    String isSmoking;

    public void eat() {
        System.out.println("我吃烧烤");
    }

    public void walk() {
        System.out.println("我散步");
    }

    public void smoking() {
        System.out.println("男人抽烟");
    }

    public void getP() {
        System.out.println(this.getClass().getClassLoader().getResource(".").getPath());
        System.out.println(this.getClass().getResource("").getPath());
        System.out.println(this.getClass().getResource("/").getPath());

    }
}


class WoMan extends People {
    String isShopping;

    public void shopping() {
        System.out.println("女人逛街");
    }

    public void eat() {
        System.out.println("我喝粥");
    }

    public void walk() {
        System.out.println("我走路");
    }
}

class Test01 {
    public static void main(String[] args) {
        People p1 = new Man();
        p1.eat();
        Man m = (Man) p1;
        m.smoking();
        if (p1 instanceof Man) {
            System.out.println("我是女人");
        }
        m.getP();
    }
}