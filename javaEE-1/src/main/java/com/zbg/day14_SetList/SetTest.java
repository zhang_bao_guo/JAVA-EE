package com.zbg.day14_SetList;

import org.junit.Test;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-23
 **/
public class SetTest {
   @Test
   public void test01() {
      Set set = new LinkedHashSet();
      set.add(2);
      set.add(3);
      set.add(4);
      set.add(1);
      set.add(new Person("张保国", 25));
      set.add(new Person("张保国", 25));
      set.add(5);
      Iterator iterator = set.iterator();
      while (iterator.hasNext()) {
         System.out.println(iterator.next());
      }
   }
}
