package com.zbg.day01_ifSwitch;

import org.junit.Before;
import org.junit.Test;

/**
 * @Author Zoe
 * @Date: 2021/02/05
 * @Description
 */

/**
 * if else: 判断语句中只要有一个为true，后续的就不会执行。终止这次判断
 * switch: 必须要加上break跳出当前的流程控制，不加break就不会跳出switch结构，
 *          不管后面条件是否符合都会执行，直到遇到第一个break才会跳出switch结构
 */
public class IfSwitchTest {
    @Test
    public void T1() {
        int i = 100;
        ifMethod(i);
        switchMethod(i);
    }

    public void ifMethod(int i) {
        if (i > 60) {
            System.out.println("及格");
        } else if (i > 100) {
            System.out.println("满分");
        } else {
            System.out.println("啦啦");
        }
    }

    public void switchMethod(int i) {
        switch (i) {
            case 60:
                System.out.println(i);
                break;
            case 100:
                System.out.println(i);
                break;
            default:
                System.out.println(i);
        }
    }
}
