package com.zbg.day22_proxyTest;

import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-24
 **/
interface Person {
    public void eat(String food);
}

class Man implements Person {
    @Override
    public void eat(String food) {
        System.out.println("我喜欢吃" + food);
    }
}

class Restaurant implements InvocationHandler {
    private Object object;
    public void getObj(Object obj){
        object = obj;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("饭店在做饭");
        Object invoke = method.invoke(object, args);
        System.out.println("饭店在吃饭");
        return invoke;
    }
}

public class ProxyTest01 {

    public static Object getObj(Object obj){
        Restaurant restaurant = new Restaurant();
        restaurant.getObj(obj);
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(),obj.getClass().getInterfaces(),restaurant);
    }
    @Test
    public void T1(){
        Man man = new Man();
        Person obj = (Person) ProxyTest01.getObj(man);
        obj.eat("麻辣浪");

    }
}
