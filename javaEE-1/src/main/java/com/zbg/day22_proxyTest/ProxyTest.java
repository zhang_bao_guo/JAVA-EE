package com.zbg.day22_proxyTest;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-19
 **/
interface Human {
    void eat(String food);

    void inter();
}

class SuperMan implements Human {

    @Override
    public void eat(String food) {
        System.out.println("我喜欢吃" + food);
    }

    @Override
    public void inter() {
        System.out.println("我喜欢打篮球");
    }
}

public class ProxyTest implements InvocationHandler {
    public Object object;

    public void getObject(Object object) {
        this.object = object;
    }

    public static Object getObj(Object obj) {
        ProxyTest proxyTest = new ProxyTest();
        proxyTest.getObject(obj);
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), proxyTest);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object invoke = method.invoke(object, args);
        return invoke;
    }
}

class Test1 {
    public static void main(String[] args) {
        SuperMan superMan = new SuperMan();
        Human obj = (Human) ProxyTest.getObj(superMan);
        obj.eat("麻辣烫");
    }
}