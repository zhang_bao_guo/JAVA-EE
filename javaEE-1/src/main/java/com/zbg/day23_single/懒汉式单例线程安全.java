package com.zbg.day23_single;

import com.sun.corba.se.spi.ior.iiop.IIOPFactories;
import org.junit.Test;

/**
 * @program: JAVA-EE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-07-27
 **/
public class 懒汉式单例线程安全 {
    @Test
    public void getInstance(){
        Singletonehan1 instance = Singletonehan1.getInstance();
        Singletonehan1 instance1 = Singletonehan1.getInstance();
        System.out.println(instance==instance1);

    }
}

class Singletonehan1 {
    private Singletonehan1() {

    };
    private static Singletonehan1 singletonehan1 = null;
    public static Singletonehan1 getInstance(){
        if (singletonehan1 ==null){
            synchronized (Singletonehan1.class){
                if (singletonehan1==null){
                    singletonehan1 = new Singletonehan1();
                }
            }
        }
        return singletonehan1;
    };
}