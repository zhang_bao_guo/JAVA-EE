package com.zbg.day23_single;

import org.junit.Test;

/**
 * @program: JAVA-EE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-07-27
 **/
public class 懒汉式单例线程不安全 {
    @Test
    public void getInstance() {
        Singletonehan instance = Singletonlanhan.getInstance();
        Singletonehan instance1 = Singletonlanhan.getInstance();
        System.out.println(instance == instance1);
    }

}

class Singletonlanhan {
    private Singletonlanhan() {
    }
    ;
    private static Singletonehan singletonehan = null;

    public static Singletonehan getInstance() {
        if (singletonehan == null) {
            singletonehan = new Singletonehan();
        }
        return singletonehan;
    }
}