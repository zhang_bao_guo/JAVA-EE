package com.zbg.day23_single;

import org.junit.Test;

/**
 * @program: JAVA-EE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-07-27
 **/
public class 恶汉试单例 {
    @Test
    public void getInstance(){
        Singletonehan singletonehan = Singletonehan.getSingletonehan();
        Singletonehan singletonehan1 = Singletonehan.getSingletonehan();
        System.out.println(singletonehan==singletonehan1);
    }
}

class Singletonehan {
    Singletonehan() {

    };

    private static Singletonehan singletonehan = new Singletonehan();

    public static Singletonehan getSingletonehan() {
        return singletonehan;
    }
}
