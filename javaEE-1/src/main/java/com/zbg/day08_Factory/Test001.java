package com.zbg.day08_Factory;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-03
 **/
public class Test001 {
    public static void main(String[] args) {
        ICar car= new BydFactory().getCar();
        car.run();
    }
}
