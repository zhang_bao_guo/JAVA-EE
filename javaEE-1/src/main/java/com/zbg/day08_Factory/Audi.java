package com.zbg.day08_Factory;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-03
 **/
public class Audi implements ICar {
    @Override
    public void run() {
        System.out.println("奥迪在跑");
    }
}
