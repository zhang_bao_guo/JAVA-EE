package com.zbg.day08_Factory;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-03
 **/
public interface CarFactory {
    ICar getCar();
}
