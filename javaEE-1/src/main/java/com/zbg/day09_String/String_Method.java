package com.zbg.day09_String;

import java.util.Arrays;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-06
 **/
public class String_Method {
    public static void main(String[] args) {
        String str ="HELLOWORLD";
        int length = str.length();
        System.out.println(length);
        System.out.println();

        char c = str.charAt(1);//返回字符串中指定位置的字符。 和数组一样，下标从0开始。
        System.out.println(c);//输出E
        System.out.println();

        boolean empty = str.isEmpty();//返回字符串是否为空  空==true，非空==false
        System.out.println(empty);
        System.out.println();

        String s = str.toLowerCase();//生成一个小写的字符串；
        System.out.println(s);
        System.out.println(str);
        System.out.println();

        String s1 = s.toUpperCase();//生成一个小写的字符串；
        System.out.println(s);
        System.out.println(s1);
        System.out.println();

        String s2=" helloworld ";
        System.out.println(s2);
        String s3 = s2.trim();//去除字符串前后空格，不包含中间的空格
        System.out.println(s3);
        System.out.println();

        String s4 = "helloworld";
        boolean b = s4.equalsIgnoreCase(str);//忽略大小写比较
        System.out.println(b);
        System.out.println();

        String s5 ="北京尚硅谷教育";
        String s6 = s5.substring(2);
        System.out.println(s6);
        String s7 = s6.substring(2, 5);
        System.out.println(s7);

        boolean b1 = s5.endsWith("教育");
        System.out.println(b1);
        System.out.println();

        boolean b2 = s5.startsWith("硅谷", 3);
        System.out.println(b2);

        boolean b3 = s5.contains("教育");
        System.out.println(b3);

        int i1 = s5.indexOf("尚", 10);
        System.out.println(i1);

        int i2 = s5.lastIndexOf("尚");
        System.out.println(i2);

        String s9 = "helloworlo";
        int i = 0;
        int count = 0;
        //判断某字符串出现的次数
        while (true){
            int i3 = s9.indexOf("o", i+1);
            i = i3;
            if (i3==-1){
                break;
            }else {
                count++;
            }
        }
        System.out.println(count+"次数");

        String s10="a-b-sd-r-d-g-hs-dg-h-9";
        String[] strs = s10.split("-");
        System.out.println(Arrays.toString(strs));
    }
}
