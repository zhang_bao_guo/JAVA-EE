package com.zbg.day09_String;

import java.io.UnsupportedEncodingException;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-06
 **/
public class StrngTest01 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        int i = 1;
        byte[] bytes = "asdfasd".getBytes("GBK");
        String s = new String(bytes);

        System.out.println(s);
    }
}
