package com.zbg.day09_String;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-06
 **/
public class day09_String {
    public static void main(String[] args) throws Exception {
        String str = "javaee";
        String str1 = str + "你好";
        String s = new String(str1.getBytes("UTF-8"));
        System.out.println(s);
        System.out.println(str1);

    }
}
