package com.zbg.day24_enum枚举类;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @program: JAVA-EE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-09-08
 **/
public class ResUtil extends HashMap {
    public static ResUtil success(ResCode resCode,Object object){
        ResUtil resUtil = new ResUtil();
        resUtil.put("code",resCode.getCode());
        resUtil.put("msg",resCode.getMsg());
        resUtil.put("data",object);
        return resUtil;
    }
}
class controller{
    public static void main(String[] args) {
        List<String> list = Arrays.asList("小明", "小白", "小绿");
        System.out.println(ResUtil.success(ResCode.SUCCESS, list));
    }
}