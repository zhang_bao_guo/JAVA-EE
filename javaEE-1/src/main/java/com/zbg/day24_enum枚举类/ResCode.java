package com.zbg.day24_enum枚举类;

public enum ResCode {

    SUCCESS(200,"请求成功"),
    FAILED(400,"请求失败");

    //返回的验证码
    private Integer code;
    //返回的信息
    private String msg;

    ResCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "ResCode{" +
            "code=" + code +
            ", msg='" + msg + '\'' +
            '}';
    }
}
