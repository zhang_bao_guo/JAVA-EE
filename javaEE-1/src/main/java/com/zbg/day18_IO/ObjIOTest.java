package com.zbg.day18_IO;

import java.io.*;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-10
 **/
class Person implements Serializable {
   String name;
   Integer age;

   public Person() {
   }

   public Person(String name, Integer age) {
      this.name = name;
      this.age = age;
   }

   @Override
   public String toString() {
      return "Person{" +
         "name='" + name + '\'' +
         ", age=" + age +
         '}';
   }
}

public class ObjIOTest {
   public static void main(String[] args) throws Exception {
      Person person = new Person("张三", 123);
      ObjectOutputStream ou = new ObjectOutputStream(new FileOutputStream(new File("C:\\Users\\14165\\Desktop\\新建文本文档.txt")));
      ou.writeObject(person);
      System.out.println("序列化成功");
      ObjectInputStream oi = new ObjectInputStream(new FileInputStream(new File("C:\\Users\\14165\\Desktop\\新建文本文档.txt")));
      Object o = oi.readObject();
      System.out.println("反序列化成功");
      System.out.println(o.toString());
   }
}
