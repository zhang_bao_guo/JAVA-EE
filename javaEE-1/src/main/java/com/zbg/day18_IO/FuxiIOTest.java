package com.zbg.day18_IO;

import org.junit.Test;

import java.io.*;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-02
 **/
public class FuxiIOTest {

    @Test
    public void Test1() throws Exception {
        File file = new File("C:\\Users\\Dowson\\Desktop\\窝窝头一块钱四个.txt");
        File file1 = new File("C:\\Users\\Dowson\\Desktop\\1111.txt");
        System.out.println(file);
        FileReader fileReader = new FileReader(file);
        FileWriter fileWriter = new FileWriter(file1);
        char[] c1 = new char[2];
        int len;
        while ((len = fileReader.read(c1)) != -1) {
            fileWriter.write(c1, 0, len);
        }
        fileWriter.close();
    }

    @Test
    public void Test2() throws Exception {
        File file = new File("C:\\Users\\Dowson\\Desktop\\窝窝头一块钱四个.txt");
        File file1 = new File("C:\\Users\\Dowson\\Desktop\\1111.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file1,false));
        String str;
        while ((str = bufferedReader.readLine()) != null) {
            bufferedWriter.write(str);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }

    @Test
    public void Test3() throws Exception {
        File file = new File("C:\\Users\\Dowson\\Desktop\\微信截图_20200302183421.png");
        File file1 = new File("C:\\Users\\Dowson\\Desktop\\微信截图.png");
        FileInputStream fileInputStream = new FileInputStream(file);
        FileOutputStream fileOutputStream = new FileOutputStream(file1);
        byte[] b = new byte[8];
        int len;
        while ((len = fileInputStream.read(b)) != -1) {
            fileOutputStream.write(b, 0, len);
        }
        fileOutputStream.close();
    }

    @Test
    public void Test4() throws Exception {
        File file = new File("C:\\Users\\Dowson\\Desktop\\微信截图_20200302183421.png");
        File file1 = new File("C:\\Users\\Dowson\\Desktop\\微信截图.png");
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file),"UTF-8");
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file1));
        byte[] b = new byte[6];
        int len;
        while ((len = bufferedInputStream.read(b)) != -1) {
            bufferedOutputStream.write(b, 0, len);
        }
        bufferedOutputStream.close();
    }
}
