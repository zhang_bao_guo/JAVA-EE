package com.zbg.day18_IO;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-10
 **/
public class SystemTest {
   public static void main(String[] args) throws Exception {
      InputStreamReader is = new InputStreamReader(System.in);
      BufferedReader bw = new BufferedReader(is);
      while (true) {
         System.out.println("请输入");
         String s = bw.readLine();
         if ("e".equalsIgnoreCase(s)) {
            System.out.println("程序结束");
            break;
         }
         System.out.println(s.toUpperCase());
      }
   }
}
