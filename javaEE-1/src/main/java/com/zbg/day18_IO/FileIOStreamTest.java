package com.zbg.day18_IO;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-08
 **/
public class FileIOStreamTest {
   @Test
   public void Test01() throws IOException {
      File file = new File("C:\\Users\\14165\\Desktop\\WeChatSetup.exe");
      File file1 = new File("C:\\Users\\14165\\Desktop\\123.exe");
      long st = System.currentTimeMillis();
      copy(file, file1);
      long ed = System.currentTimeMillis();
      System.out.println("话费时间" + (ed - st));
   }

   public void copy(File file1, File file2) throws IOException {
      FileInputStream fi = new FileInputStream(file1);
      FileOutputStream fo = new FileOutputStream(file2);
      byte[] b = new byte[1024];
      int len;
      while ((len = fi.read(b)) != -1) {
         fo.write(b, 0, len);
      }
      System.out.println("复制成功");
      fi.close();
      fo.close();
   }
}
