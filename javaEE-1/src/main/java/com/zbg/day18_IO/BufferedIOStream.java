package com.zbg.day18_IO;

import org.junit.Test;

import java.io.*;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-10
 **/
public class BufferedIOStream {
   @Test
   public void Test01() throws Exception {
      File file = new File("C:\\Users\\14165\\Desktop\\WeChatSetup.exe");
      File file1 = new File("C:\\Users\\14165\\Desktop\\123.exe");
      long st = System.currentTimeMillis();
      cpy(file, file1);
      long ed = System.currentTimeMillis();

      System.out.println("复制成功\n" + "话费时间" + (ed - st));
   }

   public void cpy(File file1, File file2) throws Exception {
      BufferedInputStream bi = null;
      BufferedOutputStream bo = null;
      try {
         bi = new BufferedInputStream(new FileInputStream(file1));
         bo = new BufferedOutputStream(new FileOutputStream(file2));
         byte[] b = new byte[1024];
         int len;
         while ((len = bi.read(b)) != -1) {
            bo.write(b, 0, len);
         }
      } catch (IOException e) {
         e.printStackTrace();
      } finally {
      }
      bi.close();
      bo.close();
   }

   @Test
   public void Test02() throws Exception {
      File file = new File("C:\\Users\\Zoe\\Desktop\\新建文本文档.txt");
      File file1 = new File("C:\\Users\\Zoe\\Desktop\\1新建文本文档.txt");
      long st = System.currentTimeMillis();
      cp1(file, file1);
      long ed = System.currentTimeMillis();

      System.out.println("复制成功\n" + "话费时间" + (ed - st));
   }

   public void cp1(File f1, File f2) throws Exception {
//      FileReader fr = new FileReader(f1);
//      FileWriter fw = new FileWriter(f2);
//      int len;
//      while ((len= fr.read())!=-1){
//         fw.write((char)len);
//      }
//      fr.close();
//      fw.close();

      BufferedReader br = new BufferedReader(new FileReader(f1));
      BufferedWriter bw = new BufferedWriter(new FileWriter(f2));
      String s;
      while ((s = br.readLine()) != null) {
         bw.write(s);
         bw.newLine();
      }
      br.close();
      bw.close();
   }
}
