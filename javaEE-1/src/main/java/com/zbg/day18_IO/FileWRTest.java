package com.zbg.day18_IO;

import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Read，Write 字符流：通常用来处理文本数据，比如 .txt结尾的文本文档
 * InputStream，OutputStream 字节流：通常用来处理字节数据，比如： 图片，视频类型的数据
 *
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-08
 **/
public class FileWRTest {
   @Test
   public void Test01() {
      // 01:File类的实体化
      File file = new File("day01\\Hello.txt");
      FileReader fr = null;
      try {
         // 02:对数据进行字符流处理
         fr = new FileReader(file);
         int read;
         // 03:read()返回读取的字符，每次读取一个。如果到达末尾则返回-1
         while ((read = fr.read()) != -1) {
            System.out.print((char) read);
         }
      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         if (fr != null) {
            try {
               fr.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
         }
      }
   }

   @Test
   public void Test02() throws IOException {
      File file = new File("day01\\Hello.txt");
      FileReader fr = null;
      try {
         fr = new FileReader(file);
         //         声明每次读取的长度是多少
         char[] chars = new char[1024];
         int len;
         // read(chars)返回读取的字符，每次返回读取的个数，如果到达末尾则返回-1
         while ((len = fr.read(chars)) != -1) {
            //         错误的写法，会导致chars数组中不足的字符不会被替换
            //         for (int i = 0; i < chars.length; i++) {
            //            System.out.print(chars[i]);
            //         }
            //         正确的写法
            for (int i = 0; i < len; i++) {
               System.out.print(chars[i]);
            }
         }
      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         fr.close();
      }
   }

   @Test
   public void Test03() throws IOException {
      FileWriter fw = null;
      try {
         File file = new File("day01\\Hello1.txt");
         fw = new FileWriter(file, true);
         fw.write("明人不说暗话，我是你爸爸");
      } finally {
         fw.close();
      }
   }

   /**
    * 复制操作
    *
    * @throws Exception
    */
   @Test
   public void Test04() throws Exception {
      File file = new File("day01\\Hello1.txt");
      File file1 = new File("day01\\Hello2.txt");
      FileReader fr = new FileReader(file);
      FileWriter fw = new FileWriter(file1);
      char[] chars = new char[8];
      int len;//返回每次读取到的字符的个数
      while ((len = fr.read(chars)) != -1) {
         //fw.write( 需要写出的数据 ,从数据的第几个位置开始写，通常是0  , 写多少个 );
         fw.write(chars, 0, len);
      }
      fw.close();
      fr.close();
   }
}
