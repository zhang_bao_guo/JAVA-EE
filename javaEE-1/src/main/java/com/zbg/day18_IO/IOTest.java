package com.zbg.day18_IO;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-06
 **/
public class IOTest {
  int i;
  int count;
  @Test
  public void Test01() throws IOException {
    File file = new File("D:\\06.IDEAWK\\javaEE\\day01\\Hello.txt");
    System.out.println(file.getAbsolutePath());
    System.out.println(file.getName());
    System.out.println(file.getPath());
    System.out.println(file.getParent());
    System.out.println(file.length());
    System.out.println(new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date(file.lastModified())));

  }

  @Test
  public void Test02() {
    File file = new File("D:\\06.IDEAWK\\javaEE");
    for (String s : file.list()) {
      System.out.println(s);
    }
    System.out.println();
  }

  @Test
  public void Test03() {
    File file = new File("D:\\06.IDEAWK\\javaEE");
    System.out.println(file.isDirectory());
    System.out.println(file.isFile());
    System.out.println(file.exists());
    System.out.println(file.canRead());
    System.out.println(file.canWrite());
  }

  /**
   * file.createNewFile创建文件
   * <p>
   * file.mkdir  判断该目录是否存在，
   * 如果父目录不存在则创建失败，
   * 如果父目录存在,子目录不存在则创建成功
   * <p>
   * file.mkdirs判断该目录是否存在，
   * 如果父目录不存在则创建失败则会先创建父目录，然后创建子目录。
   */
  @Test
  public void Test04() throws IOException {
    File file = new File("D:\\07.hah\\JAVAee");
    if (!file.exists()) {
      file.mkdirs();
      System.out.println("文件夹创建成功");
    } else {
      file.delete();
      System.out.println("删除成功");
    }
  }

  @Test
  public void Test05() throws IOException {
    File file = new File("D:\\07.hah\\JAVAee\\h1.txt");
    boolean newFile = file.createNewFile();
    File parentFile = file.getParentFile();
    File file1 = new File(parentFile, "h2.txt");
    boolean newFile1 = file1.createNewFile();

  }

  @Test
  public void Test06() {
    File file = new File("D:\\07.hah\\JAVAee");
    File[] files = file.listFiles();
    for (File file1 : files) {
      if (file1.getName().contains(".jpg")) {
        System.out.println(file1.getName());
        System.out.println("有图片文件");
        break;
      }
    }
  }

  @Test
  public void Test07() {
    File file = new File("D:\\新建文件夹");
    Test08(file);
    System.out.println((i / 1024) + "文件大小是");
    System.out.println(count);
  }

  /**
   * 递归遍历出所有的文件，且删除文件夹所有的文件
   *
   * @param file
   */
  public void Test08(File file) {
    if (file.isDirectory()) {
      for (File listFile : file.listFiles()) {
        System.out.println(listFile);
        i += listFile.length();
        count++;
        Test08(listFile);
      }
    }
//    file.delete();
  }


  @Test
  public void T1(){
    File file = new File("D:\\新建文件夹");
    T2(file);
  }


  public void T2(File file){
    if (file.isDirectory()){
      File[] files = file.listFiles();
      for (File file1 : files) {
        System.out.println(file1);
        T2(file1);
      }
    }
  }
}

