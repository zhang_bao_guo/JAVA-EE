package com.zbg.day02_Array;

import java.util.Arrays;

/**
 * @program: 二分法定位
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-11-29
 **/
public class MiddelTest {
    public static void main(String[] args) {
        int [] str = {1,3,5,2,6,4}; // 5
        int [] nums = new int[str.length];
        for (int i = 0; i < str.length; i++) {
            for (int j = i +1; j < nums.length; j++) {
                if (str[i]>str[j]){
                    int q = str[i];
                    str[i] = str[j];
                    str[j] = q;

                }
            }
        }
        nums  = str;
        System.out.println(Arrays.toString(str));

        int st = 7;
        int start = 0;
        int end = nums.length-1;
        boolean flog = true;
        while (start<=end){
            int mid = (start+end)/2;
            if (st == nums[mid]){
                flog = false;
                System.out.println("找到了，位置是"+mid);
                break;
            }else if(st > nums[mid]){
                start = mid +1;
            }else {
                end = mid-1;
            }
        }
        if (flog){
            System.out.println("没找到");
        }
    }
}
