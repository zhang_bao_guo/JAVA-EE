package com.zbg.day02_Array;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-11-29
 **/
public class ArrayArrayTest {
    public static void main(String[] args) {
        int [][] nums = {{3,5,8},{12,9},{7,0,6,4}};
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                sum+=nums[i][j];
            }
        }
        System.out.println(sum);
    }
}
