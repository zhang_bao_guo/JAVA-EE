package com.zbg.day02_Array;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-11-28
 **/
public class ArrayTest {
    public static void main(String[] args) {
        String [] str = {"a","b","d","c"};

        String [][] Str1 = new String[3][1];
        System.out.println(Str1[1]);

        for (int i = str.length-1; i >=0; i--) {
            System.out.println(str[i]+"1");
        }


        for (int i = 0; i <str.length ; i++) {
            System.out.println(str[i]);
        }

        char [] chars = new char[5];
        for (int i = 0; i < chars.length; i++) {
            System.out.println(chars[i]);
        }
    }
}
