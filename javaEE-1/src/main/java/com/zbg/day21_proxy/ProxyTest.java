package com.zbg.day21_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @program: javaEE
 * @description: 动态代理
 * @author: Dawson.Zhang
 * @create: 2020-02-17
 **/
interface Human {
    String getelif();

    void eat(String food);
}

//被代理类
class SuperMan implements Human {
    @Override
    public String getelif() {
        return "我相信我可以飞";
    }

    @Override
    public void eat(String food) {
        System.out.println("我喜欢吃" + food);
    }
}

class ProxyFactory {
    public static Object getProxyInstence(Object obj) {
        MyInvocationHandler myInvocationHandler = new MyInvocationHandler();
        myInvocationHandler.hander(obj);
        Object o = Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), myInvocationHandler);
        return o;
    }
}

class MyInvocationHandler implements InvocationHandler {
    private Object object;
    public void hander(Object object){
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        System.out.println(proxy+"=="+1);
        System.out.println(method.getName()+"=="+2);
        Object returnValue = method.invoke(object, args);
        return returnValue;
    }
}

public class ProxyTest {
    public static void main(String[] args) {
        Human proxyInstence = (Human) ProxyFactory.getProxyInstence(new SuperMan());
        proxyInstence.eat("四川麻辣烫");
        String getelif = proxyInstence.getelif();
        System.out.println(getelif);

    }
}
