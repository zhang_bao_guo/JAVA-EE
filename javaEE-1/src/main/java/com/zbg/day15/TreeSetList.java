package com.zbg.day15;

import org.junit.Test;

import java.util.*;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-28
 **/
public class TreeSetList {
   @Test
   public void Test01() {
      TreeSet set = new TreeSet();
      set.add(new User("Tom", 12));
      set.add(new User("Kit", 12));
      set.add(new User("Jeck", 12));
      set.add(new User("Mary", 12));
      set.add(new User("Sit", 12));
      Iterator iterator = set.iterator();
      while (iterator.hasNext()) {
         System.out.println(iterator.next());
      }


   }

   @Test
   public void Test02() {
      ArrayList arrayList = new ArrayList();
      arrayList.add("1");
      arrayList.add("2");
      arrayList.add("3");
      arrayList.add("4");
      Object[] objects = arrayList.toArray();
      for (int i = 0; i < objects.length; i++) {
         System.out.println(objects[i]);
      }
      List<String> arrList = Arrays.asList("1", "2", "搞啥");
      System.out.println(arrList);
   }
}

class User implements Comparable {
   private String name;
   private Integer age;

   public User() {
   }

   public User(String name, Integer age) {
      this.name = name;
      this.age = age;
   }

   @Override
   public String toString() {
      return "User{" +
         "name='" + name + '\'' +
         ", age=" + age +
         '}';
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      User user = (User) o;

      if (name != null ? !name.equals(user.name) : user.name != null) return false;
      return age != null ? age.equals(user.age) : user.age == null;
   }

   @Override
   public int hashCode() {
      int result = name != null ? name.hashCode() : 0;
      result = 31 * result + (age != null ? age.hashCode() : 0);
      return result;
   }

   @Override
   public int compareTo(Object o) {
      if (o instanceof User) {
         User user = (User) o;
         return this.name.compareTo(user.name);
      } else {
         throw new RuntimeException("类型不匹配");
      }
   }
}