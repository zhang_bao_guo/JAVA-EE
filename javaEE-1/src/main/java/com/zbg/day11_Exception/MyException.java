package com.zbg.day11_Exception;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-10
 **/
public class MyException extends RuntimeException {
   String code;

   public MyException() {
   }

   public MyException(String message, String code) {
      super(message);
      this.code = code;
   }
}

class Test01 {
   int age;

   public int getAge() {
      return age;
   }

   public void setAge(int age) {
      if (age < 0) {
         throw new MyException("年龄不合适","101");
      } else {
         this.age = age;
      }
   }

   public static void main(String[] args) {
      try {
         new Test01().setAge(-1);
      } catch (MyException e) {
         System.out.println(e.code);
      }
   }

}