package com.zbg.day11_Exception;

import org.junit.Test;

/**
 * F7 进方法， F8下一步，F9  退出
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-15
 **/
public class DebugTest {
    @Test
    public void testStringBuffer() {
        String str = null;
        StringBuffer sb = new StringBuffer();
        sb.append(str);
        say();
        System.out.println(sb.length());
        System.out.println(sb);
        StringBuffer sb1 = new StringBuffer(str);
        System.out.println(sb1);
    }

    public void say() {
        System.out.println("hello");
    }
}
