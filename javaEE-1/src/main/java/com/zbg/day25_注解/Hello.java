package com.zbg.day25_注解;

import org.junit.Test;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.METHOD,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Hello {
    String value() default "张三";
}
