package com.zbg.day25_注解;

import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @program: JAVA-EE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-09-08
 **/
@Hello(value = "张保国")
public class AnnotationTest {
    @Hello(value = "小黑")
    public String name;

    public AnnotationTest() {
    }

    public AnnotationTest(String name) {
        this.name = name;
    }

    public String show(String name) {
        return "我叫" + name;
    }

    @Override
    public String toString() {
        return "AnnotationTest{" +
            "name='" + name + '\'' +
            '}';
    }
}

class hi {
    public static void main(String[] args) throws Exception {
        Class<AnnotationTest> annotationTestClass = AnnotationTest.class;
        //获取构造方法
        Constructor<AnnotationTest> constructor = annotationTestClass.getConstructor(String.class);
        //通过构造方法获取实例
        AnnotationTest xb = constructor.newInstance("小白");
        System.out.println(xb);
        //给指定属性赋值
        Field name = annotationTestClass.getDeclaredField("name");
        name.set(xb, "小黑");
        System.out.println(xb);
        //调用指定方法
        Method show = annotationTestClass.getDeclaredMethod("show", String.class);
        System.out.println(show.invoke(xb, "张无忌"));
        //获取类注解属性值
        Hello annotation = annotationTestClass.getAnnotation(Hello.class);
        System.out.println(annotation.value());
        //获取属性上的注解属性值
        Field name1 = annotationTestClass.getDeclaredField("name");
        Hello annotation1 = name1.getAnnotation(Hello.class);
        System.out.println(annotation1.value());
    }
}