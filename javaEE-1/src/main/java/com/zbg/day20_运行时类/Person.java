package com.zbg.day20_运行时类;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-17
 **/
@MyAnnotation(value = "hi")
public class Person extends Creater<String> implements Comparable<String>, MyInterface {
    private String name;
    int age;
    public int id;

    public Person() {
    }
    @MyAnnotation(value = "abc")
    private Person(String name) {
        this.name = name;
    }

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    @MyAnnotation
    private String showNation(String nation) {
        System.out.println("我的国籍是" + nation);
        return nation;
    }

    public String display(String interest) {
        return interest;
    }
    public void say(){
        System.out.println("hi");
    }

    @Override
    public int compareTo(String o) {
        return 0;
    }

    @Override
    public void info() {
        System.out.println("我是一个人");
    }
}
