package com.zbg.day20_运行时类;

import java.io.Serializable;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-17
 **/
public class Creater<T> implements Serializable {
  private char gender;
  public double weight;

  private void breath() {
    System.out.println("生物呼吸");
  }

  public void eat() {
    System.out.println("生物进食");
  }
}
