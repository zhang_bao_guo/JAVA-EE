package com.zbg.day20_运行时类;

import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-17
 **/
public class MethodTest {
    @Test
    public void Test1() {
        Class<Person> clazz = Person.class;
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            System.out.println(method);
        }
        System.out.println();
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            int modifiers = declaredMethod.getModifiers();
            System.out.println(Modifier.toString(modifiers));
            System.out.println(declaredMethod);
        }
    }

    @Test
    public void Test2() throws NoSuchMethodException {
        Class<Person> clazz = Person.class;
        Method[] declaredMethods = clazz.getDeclaredMethods();
//        for (Method declaredMethod : declaredMethods) {
//            System.out.println(declaredMethod);
//            Annotation[] annotations = declaredMethod.getAnnotations();
//            for (Annotation annotation : annotations) {
//                System.out.println(annotation);
//            }
//            Class<?> returnType = declaredMethod.getReturnType();
//            System.out.println(returnType.getName());
//            System.out.println();
//            System.out.println(declaredMethod.getName());
//        }
    }

}
