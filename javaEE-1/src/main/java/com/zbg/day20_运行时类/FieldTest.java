package com.zbg.day20_运行时类;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-17
 **/
public class FieldTest {
    @Test
    public void Test1() {
        Class<Person> clazz = Person.class;
        //getFields:    获取当前类及其父类中权限为public的属性
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            System.out.println(field);
        }
        System.out.println();
        //getDeclaredFields:    获取当前运行时类所有声明的属性，跟权限无关。不包括父类。
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            System.out.println(declaredField);
        }
    }
    @Test
    public void Test2(){
        Class<Person> clazz = Person.class;
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            //1:返回权限修饰符
            int modifiers = declaredField.getModifiers();
            System.out.println(Modifier.toString(modifiers));
            //2:返回数据类型
            Class<?> type = declaredField.getType();
            System.out.println(type+"--"+type.getName());
            //3:获取属性名
            String name = declaredField.getName();
            System.out.println(name);
            System.out.println();
        }
    }
}
