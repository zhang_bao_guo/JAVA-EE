package com.zbg.day10;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-01-15
 **/
public class SystemData {
    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();
        System.out.println(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY:MM:dd HH:mm:ss");
        String format1 = simpleDateFormat.format(date);
        System.out.println(format1);
        Thread.sleep(1000);
        System.out.println();
        Date date1 = new Date();
        System.out.println(date);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("YYYY:MM:dd HH:mm:ss");
        String format2 = simpleDateFormat1.format(date1);
        System.out.println(format2);
        int i = format1.compareTo(format2);
        System.out.println(i);
    }

}
