package com.zbg.day01_for;

import java.util.Scanner;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-11-28
 **/
public class WhileTrueTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int z = 0;
        int f = 0;
        for(;;){
            int next = scanner.nextInt();
            if (next > 0){
                z++;
            }else if (next < 0){
                f++;
            }else {
                break;
            }

        }
        System.out.println("z"+z+"f"+f);
    }
}
