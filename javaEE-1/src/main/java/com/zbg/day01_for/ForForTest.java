package com.zbg.day01_for;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-11-28
 **/
public class ForForTest {
    public static void main(String[] args) {

        for (int i = 1; i <= 9; i++) {//99乘法表
            for (int j = 1; j <= i; j++) {
                System.out.print(j+"*"+i+"="+j*i+" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 1; i < 6; i++) { //倒三角形
            for (int j =1; j <=6-i;j++){
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println();
        for (int i =1 ; i < 7 ;i++){
            for (int j = 1; j <=i ; j++) {//正三角
                System.out.print("*");
            }
            System.out.println();
        }
    }
}