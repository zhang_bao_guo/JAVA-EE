package com.zbg.day01_for;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-11-28
 **/
public class WhileTest {
    public static void main(String[] args) {
        int i = 1;
        while (i <= 100){
            if (i % 2==0){
                System.out.println(i);
            }
            i++;
        }
        for (;;){
            System.out.println(1);
            if (i==100){
                System.out.println(1);
            }
        }
    }
}
