package com.zbg.day01_for;

import java.util.Scanner;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-11-27
 **/
public class Test01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入第一个整数");
        int i = sc.nextInt();
        System.out.println("请输入第二个整数");
        int n = sc.nextInt();

        int min = i < n ? i : n;
        for (int j = min; i > 1 ; j--){
            if (i % j ==0 && n % j ==0){
                System.out.println("公约数"+j);
                break;
            }
        }
        int max = i > n ? i : n;
        for (int j = max; j < i * n; j++) {
            if (j % i == 0 && j % n == 0) {
                System.out.println("公bei数" + j);
                break;
            }
        }

    }
}
