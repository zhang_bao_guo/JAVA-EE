package com.zbg.day17_fanxing;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-05
 **/
public class Oder<T>  implements Callable {
   String Str;
   T setId;

   public Oder() {
   }

   public Oder(String str, T setId) {
      Str = str;
      this.setId = setId;
   }
   
   public static  <E> List<E> Test01(E [] arr){
    List<E> list =   new ArrayList<E>();
      for (E e : arr) {
         list.add(e);
      }
      return list;
   }

   public static void main(String[] args) {
      Oder<Integer> objectOder = new Oder<>();
      objectOder.setId=1;
      objectOder.Str="你在搞什么";
      List<Integer> integers = Test01(new Integer[]{1, 2, 4, 5});
      System.out.println(integers);
   }

   @Override
   public Object call() throws Exception {
      System.out.println("1");
      return null;
   }
}

class Tests{
   public static void main(String[] args) throws Exception {
      ExecutorService executorService = Executors.newFixedThreadPool(10);
      Oder<Object> objectOder = new Oder<>();
      Future submit = executorService.submit(objectOder);
      submit.get();
      executorService.shutdown();
   }
}