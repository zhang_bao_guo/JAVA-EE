package com.zbg.day17_fanxing;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-05
 **/
public class MyException extends RuntimeException {
   String msg;
   int code;

   public MyException() {
   }

   public MyException(String message, String msg, int code) {
      super(message);
      this.msg = msg;
      this.code = code;
   }
}

class Student{
   private int id;

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
      if(id <= 0){
         throw new MyException("不能输入负数","不能输入负数",101);
      }
   }

   public Student() {
   }
}

class TEst{
   public static void main(String[] args) {
      Student student = new Student();
      try {
         student.setId(-1);
      }catch (MyException e){
         e.printStackTrace();
      }
   }
}