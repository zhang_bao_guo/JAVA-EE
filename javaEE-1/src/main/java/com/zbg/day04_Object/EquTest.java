package com.zbg.day04_Object;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiPredicate;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-12-25
 **/
class Student {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    @Override
    public String toString() {
        return "Student{" +
            "name='" + name + '\'' +
            ", age=" + age +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age &&
            Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}

public class EquTest {
    public static void main(String[] args) {
        Student s1 = new Student("张飒案", 1);
        Student s2 = new Student("张飒案", 1);
        Student s4 = new Student();

        System.out.println(s1.hashCode() == s2.hashCode());
        System.out.println(s1.equals(s2));
        System.out.println("-------------------------");
        System.out.println(new String("1").equals(new String("1")));
        System.out.println(LocalDateTime.now().getYear());
        boolean s = eq(s1, s2, (s3, s5) -> Optional.ofNullable(s5).isPresent());
        System.out.println(Optional.ofNullable(s4).orElse(s2));
        System.out.println(s);
    }

    public static boolean eq(Student s1, Student s2, BiPredicate<Student, Student> predicate) {
        return predicate.test(s1, s2);
    }
}
