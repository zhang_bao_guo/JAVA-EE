package com.zbg.day04_Object;

import org.junit.Test;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2019-12-25
 **/
public class JunitTest {
    @Test
    public void stu() {
        System.out.println(Students.getStu().toString());
        System.out.println(Students.getStu().toString());

    }
}

class Students{
    private Students(){

    }
    private static Students s = null;

    public static Students getStu(){
        synchronized (Students.class){
            if (s == null){
                s = new Students();
            }
        }
        return s;
    }
}