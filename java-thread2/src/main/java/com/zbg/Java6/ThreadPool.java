package com.zbg.Java6;

import java.util.concurrent.*;

/**
 * @program: hellojava8
 * @description: 创建线程方式四：使用线程池
 * @author: Dawson.Zhang
 * @create: 2019-11-26
 **/

/**
 * 实现Runnable接口
 */
class NumberPool implements Runnable {

    @Override
    public void run() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {
                sum += i;
                System.out.println(Thread.currentThread().getName() + "---" + i);

            }
        }

    }
}

/**
 * 实现Callable接口
 */
class NumberPool1 implements Callable {
    @Override
    public Object call() throws Exception {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            if (i % 2 != 0) {
                sum += i;
            }
        }
        System.out.println("总和"+sum);
        return sum;
    }
}

public class ThreadPool {
    public static void main(String[] args) {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        threadPoolExecutor.execute(new NumberPool());
        Future submit = threadPoolExecutor.submit(new NumberPool1());
        try {
            System.out.println(submit.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }
}
