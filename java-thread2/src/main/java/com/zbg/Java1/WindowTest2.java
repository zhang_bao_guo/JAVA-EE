package com.zbg.Java1;

/**
 * 懒汉设计模式
 *
 * @program: hellojava8
 * @description: 继承Thread使用同步代码块来解决线程安全问题
 * @author: Dawson.Zhang
 * @create: 2019-11-19
 **/
class Window01 extends Thread {
    private static Integer tiket = 100;
    private static Object obj = new Object();


    @Override
    public  void run() {
        while (true) {
            synchronized (this.getClass()) {
                if (tiket > 0) {
                    try {
                        sleep(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "----卖出的火车票号为----" + tiket);
                    tiket--;
                } else {
                    break;
                }
            }
        }

    }
}

public class WindowTest2 {
    public static void main(String[] args) {
        Window01 window01 = new Window01();
        Window01 window02 = new Window01();
        Window01 window03 = new Window01();
        window01.setName("窗口一");
        window01.start();
        window02.setName("窗口二");
        window02.start();
        window03.setName("窗口三");
        window03.start();

    }
}


