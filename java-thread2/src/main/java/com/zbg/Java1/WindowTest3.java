package com.zbg.Java1;

/**
 * @program: hellojava8
 * @description: 使用同步方法解决 线程安全问题
 * @author: Dawson.Zhang
 * @create: 2019-11-19
 **/
class Window3 implements Runnable {
    private Integer tiket = 100;
    private boolean flog = true;
    public synchronized void show() {
        if (tiket > 0) {
            try {
                Thread.currentThread().sleep(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "----卖出的火车票号为----" + tiket);
            tiket--;
        }else{
            flog=false;
        }
    }

    @Override
    public void run() {
        while (flog) {
            show();
        }
    }
}

public class WindowTest3 {
    public static void main(String[] args) {
        Window3 w = new Window3();
        Thread thread1 = new Thread(w);
        thread1.setName("窗口1");
        thread1.start();
        Thread thread2 = new Thread(w);
        thread2.setName("窗口2");
        thread2.start();
        Thread thread3 = new Thread(w);
        thread3.setName("窗口3");
        thread3.start();
    }
}
