package com.zbg.Java1;

/**
 * @program: hellojava8
 * @description: 使用同步方法解决线程安全问题-->继承
 * @author: Dawson.Zhang
 * @create: 2019-11-19
 **/
class Window4 extends Thread {
    private static Integer tiket = 100;
    private static Object obj = new Object();


    @Override
    public void run() {
        while (true) {
            show();
        }
    }
    public static synchronized void show () {
        if (tiket > 0) {
            try {
                Thread.currentThread().sleep(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "----卖出的火车票号为----" + tiket);
            tiket--;
        }
    }
}

public class WindowTest4 {
    public static void main(String[] args) {
        Window4 w1 = new Window4();
        w1.setName("窗口一");
        w1.start();
        Window4 w2 = new Window4();
        w2.setName("窗口二");
        w2.start();
        Window4 w3 = new Window4();
        w3.setName("窗口三");
        w3.start();
    }
}
