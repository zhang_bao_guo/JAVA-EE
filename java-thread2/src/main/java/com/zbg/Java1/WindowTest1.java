package com.zbg.Java1;

/**
 * @program: hellojava8
 * @description: 实现Runnable同步代码块
 * @author: Dawson.Zhang
 * @create: 2019-11-19
 **/
class Window implements Runnable{
    private Integer tiket = 100;

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                if (tiket > 0) {
                    try {
                        Thread.currentThread().sleep(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "----卖出的火车票号为----" + tiket);
                    tiket--;
                } else {
                    break;
                }

            }
        }
    }
}

public class WindowTest1 {
    public  static void main(String[] args) {
        Window w = new Window();
        Thread thread1 = new Thread(w);
        thread1.setName("窗口1");
        thread1.start();
        Thread thread2 = new Thread(w);
        thread2.setName("窗口2");
        thread2.start();
        Thread thread3 = new Thread(w);
        thread3.setName("窗口3");
        thread3.start();
    }
}
