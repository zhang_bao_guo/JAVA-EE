package com.zbg.exer;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: hellojava8
 * @description: 储户的多线程存款
 * @author: Dawson.Zhang
 * @create: 2019-11-22
 **/
class Account {
    private Double banlanc;

    private ReentrantLock lock = new ReentrantLock(true);
    public Account(Double banlanc) {
        this.banlanc = banlanc;
    }

    public  void posit(Double amt) {
        try {
            lock.lock();
            banlanc += amt;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "存钱成功,余额为：" + banlanc);
        } finally {
            lock.unlock();
        }
    }
}

class Consumer implements Runnable {
    private Account acc;

    public Consumer(Account acc) {
        this.acc = acc;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            acc.posit(1000.0);
        }
    }
}

public class AccountTest {
    public static void main(String[] args) {
        Account acc = new Account(0.0);
        Consumer c1 = new Consumer(acc);
        Thread t1 = new Thread(c1);
        Thread t2 = new Thread(c1);
        t1.setName("甲");
        t2.setName("乙");
        t1.start();
        t2.start();

    }
}
