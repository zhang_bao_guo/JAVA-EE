package com.zbg.Java3;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 面试题：
 *      synchronized 和 lock 的异同
 *       相同：
 *          1：都是为了解决线程安全问题；
 *
 *       不同：
 *          1：synchronized机制：
 *             在执行完相应的代码机制的时候，会自动释放代码监视器
 *          2：lock机制：
 *             需要手动的启动同步（lock.lock方法），手动释放（lock.unlock方法）。
 *
 *         使用的优先顺序： lock --> 同步代码块 --> 同步方法
 *
 *
 * @program: hellojava8
 * @description: Lock解决线程安全问题
 * @author: Dawson.Zhang
 * @create: 2019-11-22
 **/
class window4 implements Runnable {
    private Integer tiket = 100;


    private  ReentrantLock lock = new ReentrantLock(true);
    @Override
    public void run() {
        while (true) {
            try {
                lock.lock();
                if (tiket > 0) {
                    System.out.println(Thread.currentThread().getName() + "---卖出的票号为---" + tiket);
                    tiket--;
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    break;
                }
            } finally {
                lock.unlock();
            }
        }
    }
}

public class LockTest {
    public static void main(String[] args) {
        window4 w = new window4();
        Thread t1 = new Thread(w);
        Thread t2 = new Thread(w);
        Thread t3 = new Thread(w);
        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");
        t1.start();
        t2.start();
        t3.start();
    }
}
