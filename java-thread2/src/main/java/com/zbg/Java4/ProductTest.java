package com.zbg.Java4;

/**
 * @program: hellojava8
 * @description: 生产者/消费者问题
 * @author: Dawson.Zhang
 * @create: 2019-11-25
 **/

class Clerk {
    private Integer bread = 0;

    public synchronized void clerkPro() {

        if (bread < 20) {
            bread++;
            System.out.println(Thread.currentThread().getName() + "开始生产第" + bread + "个面包");
            notify();
        } else {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void clerkCon() {
        if (bread > 0) {
            System.out.println(Thread.currentThread().getName() + "开始消费第" + bread + "个面包");
            bread--;
            notify();
        } else {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Procucer extends Thread {
    private Clerk clerk;

    public Procucer(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clerk.clerkPro();
        }
    }
}

class Consumer extends Thread {
    private Clerk clerk;

    public Consumer(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clerk.clerkCon();
        }
    }
}

public class ProductTest {
    public static void main(String[] args) {
        Clerk clerk = new Clerk();
        Procucer procucer = new Procucer(clerk);
        Consumer consumer = new Consumer(clerk);
        procucer.setName("生产者");
        consumer.setName("消费者");
        procucer.start();
        consumer.start();
    }
}
