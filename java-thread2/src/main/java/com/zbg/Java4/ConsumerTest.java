package com.zbg.Java4;

/**
 * @program: hellojava8
 * @description: 生产消费问题
 * @author: Dawson.Zhang
 * @create: 2019-12-03
 **/
class Bread {
    int bread = 0;

    public synchronized void breadPro() {
        if (bread <= 20) {
            bread++;
            System.out.println("生产了第" + bread + "个面包");
            //方法运行完，释放完锁，并且唤醒等待池的线程，消费者可以来争夺锁
            notify();
        } else {
            try {
                System.out.println("面包师在等待");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    //  breadPro()  和   breadCon()锁是同一个对象，在同一时间只有一个方法能进入。
    public synchronized void breadCon() {
        if (bread > 0) {
            System.out.println("消费了第" + bread + "个面包");
            bread--;
            //方法运行完，释放锁，并且唤醒等待池里面的线程，生产者可以来争夺锁
            notify();
        } else {
            try {
                System.out.println("消费者在等待");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class BreadCon implements Runnable {
    private Bread bread;

    public BreadCon(Bread bread) {
        this.bread = bread;
    }

    @Override
    public void run() {
        while (true){
            bread.breadCon();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


class BreadPro implements Runnable {
    private Bread bread;

    public BreadPro(Bread bread) {
        this.bread = bread;
    }

    @Override
    public void run() {
        while (true){
            bread.breadPro();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class ConsumerTest {
    public static void main(String[] args) {
        Bread bread = new Bread();
        BreadPro breadPro = new BreadPro(bread);
        BreadCon BreadCon = new BreadCon(bread);

        Thread t1 = new Thread(breadPro);
        Thread t2 = new Thread(BreadCon);

        t1.setName("生产者");
        t2.setName("消费者");

        t2.start();
        t1.start();
    }
}
