package com.zbg.Java4;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 涉及到的三个方法：
 *      wait()：一旦执行了此方法，当前线程会进入阻塞状态，并释放同步锁。ps：slee()方法不会。
 *      notify():唤醒一个被wait的线程，如果有多个线程被wait了，那么会根据线程的优先级来唤醒。
 *      notifyAll():唤醒所有被wait的线程。
 *
 * 说明：
 *      wait(),notify(),notifyAll 必须使用在同步代码块，或者同步方法中。
 *      wait(),notify(),notifyAll 的调用者必须要和同步代码块或者同步方法的同步监视器保持一致。
 *      wait(),notify(),notifyAll 定义在java.lang.Object类中。
 *
 *  sleep和wait方法的异同
 *      相同：
 *          1:一旦执行方法，都可以让线程进入阻塞状态
 *      不同：
 *          1:位置不同  ->  sleep生命在Thread类中，wait生命在Object类中
 *          2:调用的要求不同   ->   sleep可以在任何需要的时候调用,wait必须使用在同步代码块，或者同步方法中。
 *          3:是否释放同步监视器   ->  如果两个方法都使用在同步代码块和同步方法中，sleep不会自动释放锁，wait会自动释放锁。
 *
 *
 * @program: hellojava8
 * @description: 线程之间的通信,使用2个线程交替打印1-100中间的数字
 * @author: Dawson.Zhang
 * @create: 2019-11-25
 **/

class Number implements Runnable {
    private Integer num = 1;
    private ReentrantLock lock = new ReentrantLock();
    @Override
    public void run() {

        while (true) {
            synchronized (this) {
                notify();
                if (num <= 100) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "--" + num);
                    num++;
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    break;
                }

            }
        }
    }
}

public class CommunicationTest {
    public static void main(String[] args) {
        Number n = new Number();
        Thread t1 = new Thread(n);
        Thread t2 = new Thread(n);
        t1.setName("线程一");
        t2.setName("线程二");
        t1.start();
        t2.start();
    }
}
