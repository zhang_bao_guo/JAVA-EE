package com.zbg.Java2;

/**
 * @program: hellojava8
 * @description: 懒汉模式线程安全
 * @author: Dawson.Zhang
 * @create: 2019-11-21
 **/
public class LazySingle {

    private LazySingle() {
    }

    private static LazySingle lazySingle = null;

    public static LazySingle getLazySingle() {
        if (lazySingle == null) {
            synchronized (LazySingle.class) {
                if (lazySingle == null) {
                    lazySingle = new LazySingle();
                    return lazySingle;
                }
            }
        }
        return lazySingle;
    }
}

class Test {
    public static void main(String[] args) {
        System.out.println(LazySingle.getLazySingle());
        System.out.println(LazySingle.getLazySingle());
    }
}