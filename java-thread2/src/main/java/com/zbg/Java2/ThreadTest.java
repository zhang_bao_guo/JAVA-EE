package com.zbg.Java2;

/**
 * 死锁的理解：
 * 不同的线程分别占用对方线程需要的同步资源，不放弃；
 * 都在等待对方放弃自己需要的同步资源，就形成了线程死锁。
 *
 * @program: hellojava8
 * @description: 演示线程死锁问题
 * @author: Dawson.Zhang
 * @create: 2019-11-21
 **/
public class ThreadTest {
    public static void main(String[] args) {
         final StringBuffer s1 = new StringBuffer();
         final StringBuffer s2 = new StringBuffer();

        new Thread(){
            @Override
            public void run() {
                synchronized (s1){
                    s1.append("a");
                    s2.append("1");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized(s2){
                    s1.append("b");
                    s2.append("2");
                    System.out.println(s1);
                    System.out.println(s2);
                }
            }
        }.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized(s2){
                    s1.append("c");
                    s2.append("3");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized(s1){
                    s1.append("d");
                    s2.append("4");
                    System.out.println(s1);
                    System.out.println(s2);
                }
            }
        }).start();
    }
}
