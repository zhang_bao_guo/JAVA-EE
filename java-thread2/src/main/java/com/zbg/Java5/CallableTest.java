package com.zbg.Java5;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @program: hellojava8
 * @description: 实现Callable接口
 * @author: Dawson.Zhang
 * @create: 2019-11-25
 **/
class Number implements Callable{
    @Override
    public Object call() throws Exception {
        int sum = 0;
        for (int i = 1; i <= 100 ; i++) {
            if (i % 2 ==0 ){
                sum +=i;
                System.out.println(i);
            }
        }
        return sum;
    }
}

public class CallableTest {
    public static void main(String[] args) {
        FutureTask futureTask = new FutureTask(new Number());
        Thread thread = new Thread(futureTask);
        thread.start();
        try {
            Object o = futureTask.get();
            System.out.println("总和为："+o);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
