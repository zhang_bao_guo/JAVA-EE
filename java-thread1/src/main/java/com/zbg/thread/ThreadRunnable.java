package com.zbg.thread;

/**
 * @program: hellojava8
 * @description: 实现Runnable接口
 * @author: Dawson.Zhang
 * @create: 2019-11-19
 **/
class Thread001 implements Runnable {
    private Integer tiket = 100;

    @Override
    public void run() {

        while (true) {
            synchronized (this) {
                if (tiket > 0) {
                    try {
                        Thread.currentThread().sleep(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "----卖出的火车票号为----" + tiket);
                    tiket--;
                } else {
                    break;
                }

            }
        }
    }
}


public class ThreadRunnable {
    public static void main(String[] args) {
        Thread.currentThread().setName("主线程");
        Thread001 thread001 = new Thread001();
        Thread thread = new Thread(thread001);
        thread.setName("窗口一");
        thread.start();
        Thread thread1 = new Thread(thread001);
        thread1.setName("窗口二");
        thread1.start();
        Thread thread2 = new Thread(thread001);
        thread2.setName("窗口三");
        thread2.start();
    }
}