package com.zbg.thread;

/**
 * @program: hellojava8
 * @description: 继承Thread卖票
 * @author: Dawson.Zhang
 * @create: 2019-11-18
 **/
class Window extends Thread {
    private static Integer tiket = 100;

    @Override
    public void run() {
        while (true) {
            if (tiket > 0) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "--卖出的票号为--" + tiket);
                tiket--;
            } else {
                break;
            }
        }
    }
}

public class WindowTest {
    public static void main(String[] args) {
        Window window1 = new Window();
        Window window2 = new Window();
        Window window3 = new Window();
        window1.setName("窗口1");
        window2.setName("窗口2");
        window3.setName("窗口3");
        window1.start();
        window2.start();
        window3.start();
    }
}
