package com.zbg.thread;

/**
 * @program: hellojava8
 * @description: 线程中方法测试
 * @author: Dawson.Zhang
 * @create: 2019-11-18
 **/
class ThreadTest01 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                try {
                    //当前线程沉睡
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //获取当前线程优先级
                System.out.println(Thread.currentThread().getPriority()+"优先级");
                //获取当前线程名称
                System.out.println(Thread.currentThread().getName() + "=" + i);
            }
        }
    }

    public ThreadTest01(String str) {
        super(str);
    }
}

public class ThreadMethodTest {
    public static void main(String[] args) {
        ThreadTest01 threadTest01 = new ThreadTest01("带参线程一");
        threadTest01.setPriority(Thread.MAX_PRIORITY);
        threadTest01.start();
        Thread.currentThread().setName("主线程");
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getPriority()+"主线程优先级");
                System.out.println(Thread.currentThread().getName() + "=" + i);
            }
//            if (i == 20) {
//                try {
//                    threadTest01.join();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }
}
