
package com.zbg.exer;

/**
 * @program: hellojava8
 * @description: 线程测试类
 * @author: Dowson.Zhang
 * @create: 2019-11-18
 **/
public class ThreadDemo {
    public static void main(String[] args) {
        ThreadDemo01 threadDemo01 = new ThreadDemo01();
        ThreadDemo02 threadDemo02 = new ThreadDemo02();
        threadDemo01.start();
        threadDemo02.start();
//        new Thread(){
//            @Override
//            public void run() {
//                for (int i = 0; i < 100; i++) {
//                    if (i % 2 == 0){
//                        System.out.println(Thread.currentThread().getName()+"i = " + i);
//                    }
//                }
//            }
//        }.start();
//        new Thread(){
//            @Override
//            public void run() {
//                for (int i = 0; i < 100; i++) {
//                    if (i % 2 != 0){
//                        System.out.println(Thread.currentThread().getName()+"i = " + i);
//                    }
//                }
//            }
//        }.start();
    }
}

class ThreadDemo01 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i % 2 ==0){
                System.out.println(Thread.currentThread().getName()+"="+i);
            }
        }
    }
}

class ThreadDemo02 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i % 2 !=0){
                System.out.println(Thread.currentThread().getName()+"="+i);

            }
        }
    }
}

