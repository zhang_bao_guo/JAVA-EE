package com.java8.option;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-02
 **/
interface Person {
    void eat();
}

class Girl implements Person {
    String name;
    String age;

    @Override
    public void eat() {
        System.out.println("我喜欢吃麻辣烫");
    }

    public Girl() {
    }

    public Girl(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}

class Boy implements Person {
    String name;
    String age;
    Girl girl;
    public Boy() {
    }

    public Boy(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public Boy(String name, String age, Girl girl) {
        this.name = name;
        this.age = age;
        this.girl = girl;
    }

    public String getName() {
        return name;
    }

    public Girl getGirl() {
        return girl;
    }

    public void setGirl(Girl girl) {
        this.girl = girl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Boy{" +
            "name='" + name + '\'' +
            ", age='" + age + '\'' +
            '}';
    }

    @Override
    public void eat() {
        System.out.println("我喜欢吃肉");
    }
}

public class OptionalTest {
    @Test
    public void T1() {
        Boy boy = new Boy();
        Optional<Boy> boy1 = Optional.ofNullable(boy);

    }

    public String getName(Boy boy) {
        Boy zhangb = Optional.ofNullable(boy).orElse(new Boy("zhangb", "23"));
        return zhangb.getName();
    }

    @Test
    public void T2() {
        Boy boy = new Boy();
        System.out.println(getName(boy));
    }

    @Test
    public void T3() {
        Optional<Boy> boy = Optional.ofNullable(null);
        if (boy.isPresent()) {
            System.out.println(boy);
        }
    }

    public Person getP(Person person) {
        return Optional.ofNullable(person).orElseGet(() -> new Girl());
    }

    @Test
    public void T4() {
        Boy zhan = new Boy("zhan", "1");
        zhan=null;
        Person p = getP(zhan);
        System.out.println(p);
    }
    @Test
    public void T5(){
        List list = new ArrayList();
        list.add("123");
        list.add("123a");
        list.add("123af");
        list.add("123afs");
        list.forEach(System.out::println);
        Boy boy = new Boy();
        boy=null;
        String g = getG(boy);
        System.out.println(g);
    }
    public String getG(Boy boy){
        Boy boy1 = Optional.ofNullable(boy).orElse(new Boy());
        Girl girl = boy1.getGirl();
        Girl girl1 = Optional.ofNullable(girl).orElse(new Girl());
        return girl1.getName();
    }
}
