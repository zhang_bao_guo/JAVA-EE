package com.java8.Lambda;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-20
 **/
public class LambdaTest1 {
    @Test
    public void Test1() {
        List<Integer> integers = get(10, new Supplier() {
            @Override
            public Object get() {
                return new Random().nextInt(10);
            }
        });
        System.out.println(integers);
        System.out.println();

        List<Integer> integers1 = get(10, () -> new Random().nextInt(100));
        integers.forEach(System.out::print);
    }

    public List<Integer> get(int count, Supplier supplier) {
        List list = new ArrayList();
        for (int i = 0; i < count; i++) {
            list.add(supplier.get());
        }
        return list;
    }

    @Test
    public void Test3() {
        Consumer c = x -> System.out.println(x);
        c.accept("你好");
        Supplier s = () -> "nmsl";
        Object o = s.get();
        System.out.println(o);
    }

    @Test
    public void Test2() {
        List<String> list1 = new ArrayList<>();
        list1.add("北京");
        list1.add("12京");
        list1.add("西京");
        list1.add("北123");
        List<String> 京 = pre(list1, x -> x.contains("京"));
        System.out.println(京);
    }

    public List<String> pre(List<String> list, Predicate<String> predicate) {
        List list1 = new ArrayList<>();
        for (String o : list) {
            if (predicate.test(o)) {
                list1.add(o);
            }
        }
        return list1;
    }
}