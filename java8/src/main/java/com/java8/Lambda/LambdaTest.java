package com.java8.Lambda;

import org.junit.Test;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @program: javaEE
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-02-20
 **/
public class LambdaTest {
    @Test
    public void Test1() {
        Runnable r1 = () -> System.out.println("你好");
        r1.run();
    }

    @Test
    public void Test2() {
        Comparator<Integer> c = (x, y) -> Integer.compare(x, y);
        int compare = c.compare(1, 3);
        System.out.println(compare);
        System.out.println();
        Comparator<Integer> c1 = Integer::compareTo;
        System.out.println(c1.compare(13, 2));

    }

    @Test
    public void Test3() {
        Function<String, String> function = x -> {
            return x;
        };
        System.out.println(function.apply("我爱北京天安门"));

        Integer fun = fun("a", "b", (x, y) -> x.compareTo(y));
        System.out.println(fun);
    }

    public Integer fun(String str, String str1, BiFunction<String, String, Integer> function) {
        Integer apply = function.apply(str, str1);
        return apply;
    }

    @Test
    public void Test4() {
        Predicate<String> predicate = x -> x.toCharArray().length > 5;
        System.out.println(predicate.test("sdfsds"));
        System.out.println(boo("你好", new String("年后")::equals));
    }


    public boolean boo(String str, Predicate predicate) {
        return predicate.test(str);
    }


    @Test
    public void T5() {
        Function function = new String("你好")::equals;
        System.out.println(function.apply("你好"));
    }


}

class Person {
    final String name = "你好";
}

class Student extends Person {
    public static void main(String[] args) {
        Student student = new Student();
    }
}