package com.java8.LambdaSimple;

import org.junit.Test;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @Author Zoe
 * @Date: 2021/04/27
 * @Description // 返回值和参数列表一模一样，就可以使用方法引用
 */
public class LambdaTest {

    @Test
    public void T1() {
        Node node = new Node();
        System.out.println(1);
        consumerFun("你好", x -> System.out.println(x));
        String s = subFun(Node::str);
        System.out.println(s);
        consumerFun("你好", System.out::println);

        String s1 = new String("你好");
        System.out.println(pri("你好", s1::equals));
    }

    public void consumerFun(String str, Consumer consumer) {
        consumer.accept(str);
    }

    public String subFun(Supplier<String> supplier) {
        return supplier.get();
    }

    public boolean pri(String i, Predicate<String> predicate) {
        return predicate.test(i);
    }
}

class Node {
    private String name;

    public static String str() {
        return "你好";
    }

    public void String(String str) {
        System.out.println(str);
    }

}
